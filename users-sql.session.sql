DROP TABLE users;
CREATE TABLE users(
    id SERIAL primary key,
    username VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    role VARCHAR(255) NOT NULL
);
INSERT INTO users(username, password, role)
values (
        'sales1@fork.com',
        '$2a$10$9KArMII.gusrgwed84CdeeTM4qKZzyozlkz.8MAynyA8Hb2e/d4cW',
        'Sales'
    ),
    (
        'purchasing1@fork.com',
        '$2a$10$GoBoeVKbIMcOjguwyYVx9.UqbPeeJ2he5D2iw7ADMyWKBD/arHMr6',
        'Purchasing'
    )
SELECT *
FROM users --------