import express, { Request, Response, NextFunction } from 'express';
import expressSession from 'express-session';
import { Client } from 'pg';
import path from 'path';
import { isLoggedIn, isLoggedInAPI } from './guard';
// import grant from 'grant';
import multer from 'multer';
import dotenv from 'dotenv';
import { logger } from "./logger";

// import routers
import { userRoutes } from './userRoutes';
import { clientRoutes } from './clientRoutes';
import { amendClientRoutes } from './amendClientRoutes';
import { salesCalendarRoutes } from './salesCalendarRoutes';
import { invoiceRoutes } from './invoiceRoutes';
import { editInvoiceRoutes } from './editInvoiceRoutes';






dotenv.config();

export const client = new Client({
    database: process.env.DB_NAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD
});

const app = express();

client.connect();

app.use(express.json());
app.use(express.urlencoded({ extended: true }))

//multer
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.resolve('./public/image'))
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
})
export const upload = multer({ storage })

app.use(express.urlencoded({ extended: true }));
app.use(express.json());


//config session and log details
app.use(expressSession({
    secret: 'fork your computer up',
    resave: true,
    saveUninitialized: true
}));




//addProduct Page upload image
app.post('/addproduct', upload.single('image'), async function (req, res) {
    const content = req.body;
    console.log(req.body)
    console.log(req.file?.filename)
    let fileName = req.file?.filename;
    await client.query('INSERT INTO items (name, quantity, unit_price, expired_date, image, status, entry_date) values ($1, $2, $3, $4, $5, $6, $7)', [content.itemName, content.quantity, content.price, content.expiry_date, fileName, content.status, content.entry_date]);
    res.redirect('/stockmanagement.html');
})



app.get('/addproduct', isLoggedInAPI, async function (req, res) {
    const result = await client.query('SELECT * FROM items')
    const items = result.rows;
    res.json(items)
});

app.get('/NonExpiredItems', async function (req, res) {
    const stocks = await client.query('SELECT * FROM items WHERE expired_date >NOW()')
    const inventory = stocks.rows;
    res.json(inventory)
});


app.get('/groupData', async function (req, res) {
    const result = await client.query('select name, json_agg(quantity) quantity, json_agg(entry_date) entry_date, json_agg(expired_date) expired_date, json_agg(unit_price) unit_price , json_agg(status) status from items group by 1 order by 1')
    const items = result.rows;
    res.json(items)
});


app.get('/expiredList', async function (req, res) {
    const result = await client.query('SELECT * FROM items WHERE expired_date < NOW()')
    const items = result.rows;
    res.json(items);
})


app.delete('/expiredList/:id', async function(req, res){
    const id = parseInt(req.params.id);
    if (isNaN(id)) {
        res.status(400).json({ msg: "id is not an integer" });
    }
    await client.query('DELETE from items where id = $1', [id]);
    res.json({ success: true });
})


app.get('/addInvoice', async function (req, res) {
    const result = await client.query('SELECT * FROM invoiceList')
    const items = result.rows;
    res.json(items)
})

app.delete('/addInvoice/:id', async function (req, res) {
    const id = parseInt(req.params.id);
    if (isNaN(id)) {
        res.status(400).json({ msg: "id is not an integer" });
    }
    await client.query('DELETE from invoiceList where id = $1', [id]);
    res.json({ success: true });
})


//OAuth grant
// const grantExpress = grant.express({
//     "defaults": {
//         "origin": "http://localhost:8080",
//         "transport": "session",
//         "state": true,
//     },
//     "google": {
//         "key": process.env.GOOGLE_CLIENT_ID || "",
//         "secret": process.env.GOOGLE_CLIENT_SECRET || "",
//         "scope": ["profile", "email"],
//         "callback": "/login/google"
//     }
// });

// app.use(grantExpress);


app.use((req, res, next) => {
    logger.debug(`ip: [${req.ip}], path: [${req.path}] method: [${req.method}]`);
    next();
});



//counter middleware
app.use((req: Request, res: Response, next: NextFunction) => {
    if (!req.session['counter']) {
        req.session['counter'] = 0;
    }
    req.session['counter'] += 1;
    req.session['abc'] = null;
    next();
})

//request access.log
app.use((req, res, next) => {
    const now = new Date();
    const dateString = now.getFullYear() + "-" + pad(now.getMonth() + 1) + "-"
        + pad(now.getDate()) + " "
        + pad(now.getHours()) + ":"
        + pad(now.getMinutes()) + ":"
        + pad(now.getSeconds())
    console.log(`[${dateString}] Request ${req.path}`);

    next();

})




//Routes
app.use('/', userRoutes);
app.use('/', salesCalendarRoutes)
app.use('/', clientRoutes)
app.use('/', amendClientRoutes)
app.use('/', invoiceRoutes)
app.use('/', editInvoiceRoutes)



app.use(express.static('public'));
app.use(isLoggedIn, express.static('protected'));


app.use((req, res) => {
    res.redirect('/404.html');
})



const PORT = 8080;



app.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
});


function pad(num: number) {
    return (num + "").padStart(2, "0");
}


