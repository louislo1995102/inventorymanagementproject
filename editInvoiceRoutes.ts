import express, { Request, Response } from 'express';
import { client } from './app';

export const editInvoiceRoutes = express.Router();


editInvoiceRoutes.get('/editInvoice', displayInvoiceDetails);

async function displayInvoiceDetails(req: Request, res: Response) {

    const invoiceListID = req.query.invoiceListID;
    const invoicePageDetails = (await client.query(`SELECT * FROM invoiceList WHERE id = $1`, [invoiceListID])).rows;

    let results = {
        invoicePageDetails: (invoicePageDetails[0])
    }
    console.log(results);
    res.json(results)
}


editInvoiceRoutes.post('/invoiceEdit/:id', editInvoiceDetails);

async function editInvoiceDetails(req: Request, res: Response) {

    const invoiceID = req.body.invoiceID;
    const clientName = req.body.clientName;
    const clientAddress = req.body.clientAddress;
    const product = req.body.product;
    const productUnitAmount = req.body.productUnitAmount;
    const productUnitPrices = req.body.productUnitPrices;
    const deliveryDate = req.body.deliveryDate;
    const TotalAmount = req.body.TotalAmount;
    console.log(111222221);

    //console.log(req.body);
    console.log(1111);

    let result = await client.query(`UPDATE invoiceList SET 
        invoiceID = $2, clientName = $3, clientAddress = $4, product = $5, productUnitAmount = $6, productUnitPrices = $7, deliveryDate = $8, TotalAmount = $9 WHERE id = $1;`,
        [req.params.id, invoiceID, clientName, clientAddress, product, productUnitAmount, productUnitPrices, deliveryDate, TotalAmount]);

    console.log(123)
    console.log(req.params.id)
    console.log(result)

    if (result.rowCount === 0) {
        res.status(400).json({ message: "Client Not Found" })
    } else {
        res.json({ message: "Successfully Update Profile" })
    }
}