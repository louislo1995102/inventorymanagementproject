import express, { Request, Response } from 'express';
import { client } from './app';
import { isLoggedInAPI } from './guard';

export const salesCalendarRoutes = express.Router();

salesCalendarRoutes.get('/invoiceDetails', isLoggedInAPI, async (req: Request, res: Response) => {
    let invoiceDetails = (await client.query(`SELECT * FROM invoiceList`)).rows;
    res.json(invoiceDetails)
})

