import express, { Request, Response } from 'express';
import { client } from './app';
import { User } from './models';
import { isLoggedIn } from './guard';
import { checkPassword, hashPassword } from './hash';
// import fetch from 'node-fetch';
// import crypto from 'crypto';

export const userRoutes = express.Router();


userRoutes.post('/login', async (req: Request, res: Response) => {
    const { username, password } = req.body;
    let users: User[] = (await client.query('SELECT * from users where username = $1', [username])).rows;
    const user = users[0];
    if (user && await checkPassword(password, user.password)) {
        req.session['user'] = user;
        if (user.role === "Sales") {
            res.status(200).json({ role: "Sales", success: true, message: `You have logged in as the ${user.role} Team!` });
        } else {
            res.status(200).json({ role: "Purchasing", success: true, message: `You have logged in as the ${user.role} Team!` });
        }
    } else {
        res.status(401).json({ success: false, message: "Username/Password is incorrect!" });
    }
});



userRoutes.post('/users', isLoggedIn, async (req, res) => {
    const { username, password } = req.body;

    await client.query('INSERT INTO users (username,password) values ($1,$2)',
        [username, await hashPassword(password)]);
    // res.redirect('/salesCalendar.html')
})




userRoutes.get('/logout', async (req: Request, res: Response) => {
    delete req.session['user'];
    res.redirect('/login.html')
})



userRoutes.get('/current-user', async (req, res) => {
    if (req.session['user']) {
        res.json({
            username: req.session['user'].username
        });
    } else {
        res.status(401).json({ message: "UnAuthorized" })
    }

})


    //google login
    // userRoutes.get('/login/google', async (req: Request, res: Response) => {
    //     const accessToken = req.session?.['grant'].response.access_token;

    //     const fetchRes = await fetch('https://www.googleapis.com/oauth2/v2/userinfo', {
    //         headers: {
    //             "Authorization": `Bearer ${accessToken}`
    //         }
    //     });
    //     const userInfo = await fetchRes.json();
    //     let users: User[] = (await client.query('SELECT * from users where username = $1', [userInfo.email])).rows;
    //     let user = users[0];
    //     if (!user) {
    //         const randomPassword = crypto.randomBytes(20).toString('hex');
    //         const result = await client.query('INSERT INTO users (username, password) values ($1, $2) returning id', [userInfo.email,
    //         await hashPassword(randomPassword)]);

    //         user = {
    //             username: userInfo.email,
    //             password: randomPassword,
    //             id: result.rows[0].id
    //         };
    //     }
    //     req.session['user'] = user;
    //     res.status(200).redirect('./public/salesCalendar/salesCalendar.html')
    // })
