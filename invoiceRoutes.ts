import express, { Request, Response} from 'express';
import {client} from './app'

export const invoiceRoutes = express.Router();

invoiceRoutes.get('/addInvoice',async function(req:Request,res:Response){
    const q = req.query.q; 
    // if(q){
    //     const result = await client.query('SELECT * FROM invoiceList WHERE content like $1',[`%${q}%`]);
    //     let invoices= result.rows;
    //     res.json(invoices);
    // }else{
        
        console.log("this is app get")
        const result = await client.query('SELECT * FROM invoiceList');
        let invoices = result.rows;
        res.json(invoices);
    // }

    
});

// app.get('/NotExpired',async function(req:Request,res:Response){
//     const stock = await client.query('SELECT * FROM items WHERE expiry_date > NOW()');
//     let stocks = stock.rows;
//     res.json(stocks);
// })

invoiceRoutes.get('/items',async function(req:Request,res:Response){
    const q = req.query.q; 
    // if(q){
        const itemsName = await client.query('SELECT * FROM items');
        let names = itemsName.rows;
        res.json(names);
    // }else{
        
        // console.log("this is app get")
        // const result = await client.query('SELECT * FROM invoiceList');
        // let invoices = result.rows;
        // res.json(invoices);
    // }

    
});

// invoiceRoutes.get('/invoiceList',async function(req:Request,res:Response){
//     const q = req.query.q; 
//     if(q){
//         const result = await client.query('SELECT * FROM invoiceList WHERE content like $1',[`%${q}%`]);
//         let invoices= result.rows;
//         res.json(invoices);
//     }else{
        
//         console.log("this is app get")
//         const result = await client.query('SELECT SUM(product)')
//         FROM table_name
//         WHERE condition;');
//         let invoices = result.rows;
//         res.json(invoices);
//     }

    
// });


invoiceRoutes.post('/addInvoice', async function(req:Request,res:Response){
    const invoiceID = req.body.invoiceID;
    const clientName = req.body.clientName;
    const clientAddress = req.body.clientAddress;
    const product = req.body.product;
    const productUnitAmount = req.body.productUnitAmount;
    const productUnitPrices = req.body.productUnitPrices;
    const deliveryDate = req.body.deliveryDate;
    const TotalAmount = req.body.TotalAmount;
    

    console.log(req.body)
    console.log(req.body.TotalAmount)
    // await client.query('insert into clientList (clientName, clientAddress, contactPerson, contactNumber) values ($1,$2,$3,$4)',
    //         [clientName,clientAddress,contactPerson,contactNumber]);

    let a = await client.query('insert into invoiceList(invoiceID, clientName,clientAddress,product,productUnitAmount, productUnitPrices,deliveryDate,TotalAmount) values ($1,$2,$3,$4,$5,$6,$7,$8)',
    [invoiceID, clientName,clientAddress,product,productUnitAmount, productUnitPrices,deliveryDate,TotalAmount]);
    

    console.log(a)
    res.status(200).json({success:true});
});