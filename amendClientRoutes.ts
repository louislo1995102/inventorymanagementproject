import express, { Request, Response } from 'express';
import { client } from './app';

export const amendClientRoutes = express.Router();


amendClientRoutes.get('/amendClient', displayClientDetails);

async function displayClientDetails(req: Request, res: Response) {

    const clientListID = req.query.clientListID;
    const clientPageDetails = (await client.query(`SELECT * FROM clientList WHERE id = $1`, [clientListID])).rows;

    let results = {
        clientPageDetails: (clientPageDetails[0])
    }

    console.log(results);
    res.json(results)

}



amendClientRoutes.post('/profile/:id', editClientDetails);

async function editClientDetails(req: Request, res: Response) {

    const clientName = req.body.clientName;
    const clientAddress = req.body.clientAddress;
    const contactPerson = req.body.contactPerson;
    const contactNumber = req.body.contactNumber;

    console.log(req.body)
    console.log(111111)

    let result = await client.query(`UPDATE clientList SET 
        clientName = $2, clientAddress = $3, contactPerson = $4, contactNumber = $5 WHERE id = $1;`,
        [req.params.id, clientName, clientAddress, contactPerson, contactNumber]);

    console.log(req.params.id)
    console.log(result)

    if (result.rowCount === 0) {
        res.status(400).json({ message: "Client Not Found" })
    } else {
        res.json({ message: "Successfully Update Profile" })
    }
}