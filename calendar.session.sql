DROP TABLE users;
CREATE TABLE users(
    id SERIAL primary key,
    username VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    role VARCHAR(255) NOT NULL
);
INSERT INTO users(username, password, role)
values (
        'sales1@fork.com',
        '$2a$10$9KArMII.gusrgwed84CdeeTM4qKZzyozlkz.8MAynyA8Hb2e/d4cW',
        'Sales'
    ),
    (
        'purchasing1@fork.com',
        '$2a$10$GoBoeVKbIMcOjguwyYVx9.UqbPeeJ2he5D2iw7ADMyWKBD/arHMr6',
        'Purchasing'
    )
SELECT *
FROM users --------
    DROP TABLE product CREATE TABLE product(
        id SERIAL primary key,
        name VARCHAR(255) NOT NULL,
        price INTEGER,
        image VARCHAR(255)
    ) -- ALTER TABLE product
    -- ADD CONSTRAINT purchase_id FOREIGN KEY(purchase_id) REFERENCES purchase(id)
INSERT INTO product(name, price, image)
values ('科啟月餅', 40, 'moonCake.png'),
    ('英國變種安格斯', 100, 'angus.png'),
    ('科啟彈牙QQ鮑', 80, 'haliotis.png')
SELECT *
FROM product
    join purchase on purchase.product_id = product.id;
-------------
DROP TABLE purchase CREATE TABLE purchase(
    id SERIAL primary key,
    name VARCHAR(255) NOT NULL,
    cost INTEGER,
    quantity INTEGER,
    product_id INTEGER,
    FOREIGN KEY(product_id) REFERENCES product(id)
);
INSERT INTO purchase(name, cost, quantity, product_id)
values ('科啟月餅', 20, 200, 1),
    ('英國變種安格斯', 30, 150, 2),
    ('科啟彈牙QQ鮑', 60, 300, 3)
SELECT *
FROM purchase -----------  
    DROP TABLE product_batch CREATE TABLE product_batch(
        id SERIAL primary key,
        entry_date Date,
        expiry_date Date,
        stock INTEGER,
        status VARCHAR(255),
        product_id INTEGER,
        FOREIGN KEY(product_id) REFERENCES product(id)
    )
INSERT INTO product_batch(
        entry_date,
        expiry_date,
        stock,
        status,
        product_id
    )
values ('11/06/2021', '10/8/2021', 50, 'active' 1),
    ('24/06/2021', '30/07/2021', 100, 'expired', 2),
    ('06/03/2021', '01/09/2021', 200, 'delivered', 3),
    ('09/05/2020', '08/01/2021', 30, 'expired', 3)
SELECT *
FROM product_batch -------