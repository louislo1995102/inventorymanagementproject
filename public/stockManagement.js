window.onload = () => {
    loadProduct();
}


async function loadProduct() {
    const res = await fetch('/addproduct');
    const addproduct = await res.json();
    console.log(addproduct)
    const newProduct = document.querySelector('#newProduct');



    newProduct.innerHTML = '';
    for (let product of addproduct) {
        newProduct.innerHTML += `
        <div class="row my-container" style="margin-top: 20px; align-items: center;">

            <div class="col-md-2">${product.id}</div>
            <div class="col-md-2">${product.name}</div>
            <div class="col-md-2">
                <img src="/image/${product.image}" alter="productphoto" class="img-fluid" />
            </div>
            <div class="col-md-2">${product.quantity}</div>
            <div class="col-md-2">${product.status}</div>
            <div class="col-md-2"><a href="/productStatus/ProductStatus.html"><img src="/image/StockManagementImage/edit-solid.svg" style="width: 40%; box-shadow: 10px 10px 8px #888888;" alter="edit-icon" class="img-fluid" /></a></div>
        </div>
        `
    }
}


