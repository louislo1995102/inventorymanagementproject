import { loadCurrentUser } from "./load-current-user.js";


window.onload = () => {
    loadCurrentUser();
}

document.querySelector('#login-form').onsubmit = async function (event) {
    event.preventDefault();

    const form = event.target;

    const formObj = {
        username: form.username.value,
        password: form.password.value,
    }

    const res = await fetch('/login', {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(formObj)
    })

    const result = await res.json();
    // console.log(result)
    if (res.status === 200) {
        if (result.role == "Sales") {
            window.location = '/salesCalendar/salesCalendar.html';
        } else {
            window.location = '/purchasingCalendar/purchasingCalendar.html';
        }
    } else {
        document.querySelector('#alert-container').innerHTML += `<div class="alert alert-danger" role="alert">
        Login Failed!
        </div>`;
    }
}