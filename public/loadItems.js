let NonExpiredItems = [];
let productSales = [];




async function loadRemainStock() {
  const res = await fetch("/NonExpiredItems"); // DEFAULT 係Get Method
  const stocks = await res.json();
  let product =
    document.querySelector("#product").options[
      document.querySelector("#product").selectedIndex
    ].value;

  for (let stock of stocks) {
    NonExpiredItems.push([stock.name, stock.quantity]);
  }
  // const reducer = (accumulator, currentValue) => parseInt(accumulator) + parseInt(currentValue);
  // document.querySelector("#Inventory").innerHTML = `Remaining Stock of ${product}: ${NonExpiredItems.reduce(reducer)}`;
  // document.querySelector("#Inventory").style="color:black;text-align:center;";
  console.log(NonExpiredItems);
}
async function loadProductSales() {
  const res = await fetch("/addInvoice"); // DEFAULT 係Get Method
  const sales = await res.json();
  for (let sale of sales) {
    productSales.push([sale.product, sale.productunitamount]);
  }
  // const reducer = (accumulator, currentValue) => parseInt(accumulator) + parseInt(currentValue);
  // document.querySelector("#Inventory").innerHTML = `Remaining Stock of ${product}: ${NonExpiredItems.reduce(reducer)}`;
  // document.querySelector("#Inventory").style="color:black;text-align:center;";
  console.log(productSales);
}

let productSelected = document.querySelector("#product");

productSelected.addEventListener('click', function () {
  let mp = new Map();
  let ms = new Map();
  for (let i of NonExpiredItems) {
    if (mp.has(i[0])) { //have that value
      mp.set(i[0], mp.get(i[0]) + i[1])
    }
    else {
      mp.set(i[0], i[1])
    }
  }
  for (let x of productSales) {
    if (ms.has(x[0])) { //have that value
      ms.set(x[0], ms.get(x[0]) + x[1])
    }
    else {
      ms.set(x[0], x[1])
    }
  }
  let sales;
  if (!isNaN(parseInt(ms.get(productSelected.value)))) {
    sales = parseInt(ms.get(productSelected.value));
  } else {
    sales = 0;
  }

  let inventorys;
  if (!isNaN(parseInt(mp.get(productSelected.value)))) {
    inventorys = parseInt(mp.get(productSelected.value));
  } else {
    inventorys = 0;
  }

  document.querySelector('#NonExpiredProduct').innerHTML = "Non-Expired Amount of " + `${productSelected.value}:`;
  document.querySelector('#NonExpiredInventory').value = inventorys;
  document.querySelector('#SalesOfProduct').innerHTML = "Sales Amount of " + `${productSelected.value}:`;
  document.querySelector('#ProductSales').value = sales;
  document.querySelector('#RemainingProduct').innerHTML = "Total Inventory of " + `${productSelected.value}:`;

  document.querySelector('#ProductInventory').value = parseInt(document.querySelector('#NonExpiredInventory').value) - sales;
})




// productSelected.addEventListener("click", function () {
//   let inventorys = filter(NonExpiredItems);
//   for (let inventory of inventorys) {
//     let stockStorage=[];
//     stockStorage.push(stock[1]);
//     console.log(stockStorage)
//   }

// });

let itemPrices = [];
async function loadItems() {
  const res = await fetch("/items");
  console.log(res);
  const items = await res.json();
  console.log(items);
  const itemsSelection = document.querySelector("#product");
  // itemsSelection.innerHTML += `<option>${items[0].name}</option>`;

  for (let item of items) {
    if (itemsSelection.innerHTML.includes(item.name) === false) {
      itemPrices.push(item.unit_price);
      console.log(itemPrices[1]);
      itemsSelection.innerHTML += `<option class="itemNames" id="${item.name}" value="${item.name}">${item.name}</option>`;
    }
  }
}
async function GenerateInvoiceID() {
  const res = await fetch("/addInvoice");
  console.log(res);
  const invoices = await res.json();
  const invoiceID = document.querySelector("#invoiceIdInput");
  // itemsSelection.innerHTML += `<option>${items[0].name}</option>`;

  for (let invoice of invoices) {
    let x = [];
    x.push(invoice.id);
    invoiceID.value = "21" + String(Math.max(...x) + 1).padStart(4, "0");
  }
}
let clientLocation = [];

async function loadClients() {
  const res = await fetch("/addClient");
  const clients = await res.json();
  console.log(clients);
  const clientSelection = document.querySelector("#clientNameInput");
  for (let client of clients) {
    if (clientSelection.innerHTML.includes(client.clientname) === false) {
      clientLocation.push(`${client.clientaddress}`);
      console.log(clientLocation[2]);
      clientSelection.innerHTML += `<option class="ClientNames" type="text" name="${client.clientname}" value="${client.clientname}">${client.clientname}</option>`;

      // if(document.querySelector("#clientNameInput").options[document.querySelector("#clientNameInput").selectedIndex].value=== client.name){

      // }
    }
    // if(document.querySelector("#clientNameInput").options[document.querySelector("#clientNameInput").selectedIndex].value==client.clientname){
    //   document.querySelector('#clientAddressInput').value= client.clientaddress;
    // }
  }
  // let options = document.querySelector("#clientNameInput").options;
  // for(let client of clients){
  //   for(let option of options){
  //     if(option.value===client.clientname){
  //       document.querySelector('#clientAddressInput').value= client.clientaddress
  //     }
  //   }
}

function findLocation(thisIndex) {
  document.querySelector("#clientAddressInput").value =
    clientLocation[thisIndex];
}
// async function loadLocations() {
//   const res = await fetch("/addClient");
//   const clients = await res.json();
//   for (let client of clients) {
//     if (document.querySelector("#clientNameInput").options[document.querySelector("#clientNameInput").selectedIndex].value === client.name) {
//       document.querySelector("#clientAddressInput").value =
//         client.clientaddress;
//     }
//   }
// }
// function findLocation(b) {
//   let location = document.querySelector("#clientAddressInput");
//   location.value = b;
// }

function calculateProduct(thisIndex) {
  document.querySelector("#productUnitPrices").value = itemPrices[thisIndex];
}

function calculateAmount(val) {
  var tot_price = val * document.querySelector("#productUnitPrices").value;
  /*display the result*/
  var divobj = document.getElementById("TotalAmount");
  divobj.value = `${tot_price}`;
}

// async function loadPrice(){
//     const res = await fetch("/items");
//     console.log(res)
//     const items = await res.json();
//     console.log(items);
//     const itemsSelection = document.querySelector("#product");
//     const itemPrice = document.querySelector("#productUnitPrices");
//     for(let item of items){

//         if(itemsSelection.value==`${item.name}`){
//             itemPrice.value=`${item.unit_price}`
//           }
//     }
// }

window.onload = async (event) => {
  loadItems();
  loadClients();
  GenerateInvoiceID();
  loadProductSales();
  loadRemainStock();
};
