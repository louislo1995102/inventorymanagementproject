

  



async function loadInvoices() {
  const res = await fetch("/addInvoice"); // DEFAULT 係Get Method
  const invoices = await res.json();
  console.log(res);
  const invoiceInfoTable = document.querySelector("#invoiceContentDiv");
  
  
  // clientInfoTable.innerHTML = '';
  // for (let invoice of invoices) {
  //   invoiceInfoTable.innerHTML += `        <div class="invoiceInfoContent">
  //       <a href="#"class="col-md-3 invoiceInfo InvoiceID">
  //           ${invoice.invoiceID}
  //       </a>
  //       <div class="col-md-3 invoiceInfo ClientName">
  //           ${invoice.clientName}
  //       </div>
  //       <div class="col-md-3 invoiceInfo deliveryDate">
  //           ${invoice.deliveryDate}
  //       </div>
  //       <div class="col-md-3 invoiceInfo Status">
  //           <i class="far fa-check-circle check"></i>
  //       </div>
  // //   </div>`;
  //   window.location(invoiceList.html);
  // }
}
// let x=0;
// document.querySelector('#addRow').addEventListener('click', function(){
//   x++;
//   document.querySelector('#productList').innerHTML+=`<div class="invoiceInfo ProductInfo">
//   <div class="col-md-4 productNames productColumns">
//       <select name="product" id="product${x}" onchange="calculateProduct(this.value)" readonly>
//       </select>
//   </div>
//   <div class="col-md-4 productUnits productColumns">
//       <div class="row">

//           <div class="col-md-6"><input type="text" id="productUnitAmount" name="productUnitAmount" onchange="calculateAmount(this.value)" ></div>
//           <select class="col-md-6" name="productUnit" id="productUnit" readonly>
//               <option id="box" value="box">box</option>
//           </select>
//       </div>
//   </div>
//   <div class="col-md-2 productUnitPrices productColumns">
//       <input type="text" id="productUnitPrices" name="productUnitPrices" style="width:100px; font-size:20px;" placeholder="SELECT PRODUCT" readonly>
//   </div>
//   <div class="col-md-2 productEditButtons productColumns">
//       <button id="addRow"><i  class="fas fa-plus-circle"></i></button>
//   </div>
// </div>`;  

// window.onload=()=>loadItems();
// })


document
  .querySelector("#addInvoice")
  .addEventListener("submit", async function (event) {
    event.preventDefault();
    if(parseInt(document.querySelector('#productUnitAmount').value)<=parseInt(document.querySelector('#ProductInventory').value)){
      
      const form = event.target;
      // 1. JSON Object
      // const formObj = {
      //     content: form.content.value
      // };
      // const res = await fetch('/memos',{
      //     method:"POST",
      //     headers:{
      //         "Content-Type":"application/json"
      //     },
      //     body: JSON.stringify(formObj)
      // });
      const formData = new FormData();
      formData.append("invoiceID", form.invoiceID.value);
      formData.append("clientName", form.clientName.value);
      formData.append("clientAddress", form.clientAddress.value);
      formData.append("deliveryDate", form.deliveryDate.value);
      formData.append("product", form.product.value);
      formData.append("productUnitAmount", form.productUnitAmount.value);
      formData.append("productUnitPrices", form.productUnitPrices.value);
      formData.append("TotalAmount", form.TotalAmount.value);
  
      let obj = {
        invoiceID: form.invoiceID.value,
        clientName: form.clientName.value,
        clientAddress: form.clientAddress.value,
        deliveryDate: form.deliveryDate.value,
        product: form.product.value,
        productUnitAmount: form.productUnitAmount.value,
        productUnitPrices: form.productUnitPrices.value,
        TotalAmount: form.TotalAmount.value,
      };
  
      console.log(obj);
  
      const res = await fetch("/addInvoice", {
        method: "POST",
        headers: {
          "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(obj),
      });
  
      let result = res.json();
  
      if (res.status == 200) {
        // guaranteed 返個Response 真係啱嘅response
        console.log(result);
      }
      await loadInvoices();

    }else if(parseInt(document.querySelector('#productUnitAmount').value)>parseInt(document.querySelector('#ProductInventory').value)) {
  
      window.alert('Not Enough Inventory');
    }
  });

window.onload = (event) => {
  loadInvoices();
};
