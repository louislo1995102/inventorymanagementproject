const params = new URLSearchParams(window.location.search);
const invoiceListID = parseInt(params.get('invoiceListID'));

main();
function calculateProduct(thisIndex) {
    document.querySelector("#productUnitPrices").value = itemPrices[thisIndex];
}

function calculateAmount(val) {
    var tot_price = val * document.querySelector("#productUnitPrices").value;
    /*display the result*/
    var divobj = document.getElementById("TotalAmount");
    divobj.value = `${tot_price}`;
}

async function main() {
    loadInvoiceProfile(invoiceListID);

    async function loadInvoiceProfile(invoiceListID) {
        const res = await fetch("/editInvoice" + location.search);
        const result = (await res.json()).invoicePageDetails;

        console.log(location.search)
        console.log(result)
        await displayInvoice(result)
    }
}

function displayInvoice(result) {
    document.querySelector('#invoiceIdInput').value = result.invoiceid;
    document.querySelector('#clientAddressInput').value = result.clientaddress;
    document.querySelector('#deliveryDateInput').value = `${moment(result.deliverydate).format("YYYY-MM-DD")}`
    document.querySelector('#productUnitAmount').value = result.productunitamount;
    document.querySelector('#productUnitPrices ').value = result.productunitprices;
    document.querySelector('#TotalAmount').value = result.totalamount;

    const clientNameList = document.querySelector('#clientNameInput');
    const productNames = document.querySelector('#product');
    console.log(result)


    clientNameList.innerHTML += `
    <option value="${result.clientname}">${result.clientname}</option>`;

    productNames.innerHTML += `
    <option value="${result.product}">${result.product}</option>`;

    console.log(result);



    // for (let result of results) {

    //     console.log(result)
    // }

}



const editInvoiceForm = document.querySelector('#editInvoiceForm')
editInvoiceForm.addEventListener("submit", async function (event) {
    event.preventDefault();

    const form = event.target;
    const formObj = {};

    formObj['invoiceID'] = form.invoiceID.value;
    formObj['clientName'] = form.clientName.value;
    formObj['clientAddress'] = form.clientAddress.value;
    formObj['product'] = form.product.value;
    formObj['productUnitAmount'] = form.productUnitAmount.value;
    formObj['productUnitPrices'] = form.productUnitPrices.value;
    formObj['deliveryDate'] = form.deliveryDate.value;
    formObj['TotalAmount'] = form.TotalAmount.value;

    console.log(form.TotalAmount.value);
    console.log(formObj)

    const res = await fetch(`/invoiceEdit/${invoiceListID}`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8"
        },
        body: JSON.stringify(formObj)
    })
    const result = await res.json();
    console.log(result)
})

