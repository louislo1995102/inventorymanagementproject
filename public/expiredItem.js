window.onload = () => {
  expiredPage();
  document
    .querySelector(".saveBtn")
    .addEventListener("click", async function (event) {
      event.preventDefault();
      const myCols = document.querySelectorAll(".myCols");
      for (let myCol of myCols) {
        let productChecker = myCol.querySelector(
          `input`
        );
        console.log(productChecker);
        if (productChecker)
          if (productChecker.checked == true) {

            const res = await fetch(`/expiredList/${productChecker.getAttribute("data")}`, {
              method: "DELETE",
            });
          }
      }
      expiredPage();
    });
};

async function expiredPage() {
  const res = await fetch("/expiredList");
  const addproduct = await res.json();
  console.log(addproduct);
  const expiredPage = document.querySelector("#expiredPage");



  expiredPage.innerHTML = "";
  for (let product of addproduct) {
    expiredPage.innerHTML += `
            <div class="row my-container" style="margin-top: 5px; align-items: center;">
                <div class="col-md-2">${product.id}</div>
                <div class="col-md-3">${product.name}</div>
                <div class="col-md-2">${product.quantity}</div>
                <div class="col-md-3">${moment(product.expired_date).format("YYYY-MM-DD")}</div>
                <div class="col-md-2 myCols">
                <input class="form-check-input" type="checkbox" data="${product.id}" aria-label="..." id="checkboxNoLabel${product.id}">
                </div>
            </div>
            `;
  }
}
