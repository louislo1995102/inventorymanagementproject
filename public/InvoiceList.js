

let searchSth = document.querySelector("#search");
let voiceText = document.querySelector('#output');


let NonExpiredItems = [];
let productSales = [];




async function loadRemainStock() {
  const res = await fetch("/NonExpiredItems"); // DEFAULT 係Get Method
  const stocks = await res.json();

  for (let stock of stocks) {
    NonExpiredItems.push([stock.name, stock.quantity]);
  }
  // const reducer = (accumulator, currentValue) => parseInt(accumulator) + parseInt(currentValue);
  // document.querySelector("#Inventory").innerHTML = `Remaining Stock of ${product}: ${NonExpiredItems.reduce(reducer)}`;
  // document.querySelector("#Inventory").style="color:black;text-align:center;";
  console.log(NonExpiredItems);
}
async function loadProductSales() {
  const res = await fetch("/addInvoice"); // DEFAULT 係Get Method
  const sales = await res.json();
  for (let sale of sales) {
    productSales.push([sale.product, sale.productunitamount]);
  }
  // const reducer = (accumulator, currentValue) => parseInt(accumulator) + parseInt(currentValue);
  // document.querySelector("#Inventory").innerHTML = `Remaining Stock of ${product}: ${NonExpiredItems.reduce(reducer)}`;
  // document.querySelector("#Inventory").style="color:black;text-align:center;";
  console.log(productSales);
}

async function loadProducts() {
  const res = await fetch("/items");
  console.log(res);
  const items = await res.json();
  console.log(items);
  // const productsSelection = document.querySelector("#productSearch");
  // // itemsSelection.innerHTML += `<option>${items[0].name}</option>`;

  // for (let item of items) {
  //   if (productsSelection.innerHTML.includes(item.name) === false) {
  //     productsSelection.innerHTML += `<option class="ProductName" id="${item.name}" value="${item.name}">${item.name}</option>`;
  //   }
  //   console.log(productsSelection.innerHTML);
  // }
}



async function loadInvoices() {
  const res = await fetch("/addInvoice"); // DEFAULT 係Get Method
  const invoices = await res.json();
  console.log(invoices);
  const invoiceInfoTable = document.querySelector("#invoiceContentDiv");
  // invoiceInfoTable.innerHTML = '';
  for (let invoice of invoices) {
    console.log(invoice);
    invoiceInfoTable.innerHTML += `<div class="invoiceInfoContent ${invoice.product.toLowerCase()}">
            <a href="#"class="col-md-3 invoiceInfo InvoiceID">
                ${invoice.invoiceid}
            </a>
            <div class="col-md-2 invoiceInfo ClientName">
                ${invoice.clientname}
            </div>
            <div class="col-md-3 invoiceInfo deliveryDate">
                ${moment(`${invoice.deliverydate}`).format("YYYY-MM-DD")}
            </div>
            <div id="editButton" class="col-md-2 editInfo">
        <a href="/editInvoice.html?invoiceListID=${invoice.id}">
            <i class="fas fa-edit editButton"></i>
        </a>
        </div>`;

    window.onload = (event) => {
      checkSearch();
      loadInvoices();
    };
  }
}

// searchSth.addEventListener("input", function findInvoice() {
//   for (let x of document.querySelector(".invoiceInfoContent").classList) {
//     if (x.includes(`${searchSth.value}`)) {
//       for (let y of document.getElementsByClassName(x)) {
//         y.style = "display:show;";
//       }
//     } else {
//       for (let y of document.getElementsByClassName(x)) {
//         y.style = "display:none";
//       }
//     }

//     console.log(searchSth.value.length);

//     if (searchSth.value.length === 0) {
//       for (let y of document.getElementsByClassName(x)) {
//         y.style = "display:show;";
//       }
//     }
//   }
// });
function checkSearch(){
  console.log()
  for (let xx of document.querySelectorAll(".invoiceInfoContent")){
    if (xx.classList.contains(`${searchSth.value.toLowerCase()}`)) {
      xx.style = "display:show";
    
  } else if(searchSth.value.length === 0) {
    xx.style = "display:show;";
  

}else
  {
      xx.style = "display:none";
    }
  }

  console.log(searchSth.value.length);



  }
    

  
  
searchSth.addEventListener("input", function findInvoice() {
  checkSearch();
  searchQuantity();
  });





function searchQuantity() {
  let mp = new Map();
  let ms = new Map();
  for (let i of NonExpiredItems) {
    if (mp.has(i[0])) { //have that value
      mp.set(i[0], mp.get(i[0]) + i[1])
    }
    else {
      mp.set(i[0], i[1])
    }
  }
  for (let x of productSales) {
    if (ms.has(x[0])) { //have that value
      ms.set(x[0], ms.get(x[0]) + x[1])
    }
    else {
      ms.set(x[0], x[1])
    }
  }
  let sales;
  if (!isNaN(parseInt(ms.get(searchSth.value)))) {
    sales = parseInt(ms.get(searchSth.value));
  } else {
    sales = 0;
  }

  let inventorys;
  if (!isNaN(parseInt(mp.get(searchSth.value)))) {
    inventorys = parseInt(mp.get(searchSth.value));
  } else {
    inventorys = 0;
  }
  let remaining= parseInt(inventorys)-parseInt(sales);

  document.querySelector('#QuantityCheck').innerHTML = remaining;
}



function runSpeechRecognition() {
  // get output div reference
  var output = document.getElementById("output");
  // get action element reference
  var action = document.getElementById("action");
      // new speech recognition object
      var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
      var recognition = new SpeechRecognition();
  
      // This runs when the speech recognition service starts
      recognition.onstart = function() {
          action.innerHTML = "<small>FORK IS LISTENING...</small>";
      };
      
      recognition.onspeechend = function() {
          action.innerHTML = "<small>FORK DOES'NT LISTEN TO YOU NOW</small>";
          recognition.stop();
      }
    

      recognition.onresult = function(event) {
          var transcript = event.results[0][0].transcript;
          searchSth.value = transcript;
          output.classList.remove("hide");
          checkSearch();
          searchQuantity();
      };
    
       // start recognition
       recognition.start();
       
}

document.querySelector('#stop').addEventListener('click', function(){
  
  document.querySelectorAll(".invoiceInfoContent").style="display:show";
  document.querySelector('#QuantityCheck').innerHTML = "";
  document.querySelector('#search').value="";
  
})


window.onload = (event) => {
  loadInvoices();
  loadProducts();
  loadRemainStock();
  loadProducts()
};


