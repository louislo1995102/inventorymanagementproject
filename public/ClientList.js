
async function loadClients() {

    const res = await fetch('/addClient'); // DEFAULT 係Get Method
    const clients = await res.json();
    console.log(clients)

    const clientInfoTable = document.querySelector('#clientContentDiv');
    // clientInfoTable.innerHTML = '';

    for (let client of clients) {
        console.log(client)
        clientInfoTable.innerHTML += `
        <div class="clientInfoContent">
        <a href="#" class="col-md-3 clientInfo clientName">
        ${client.clientname}
        </a>
        <div class="col-md-3 clientInfo clientAddress">
        ${client.clientaddress}
        </div>
        <div class="col-md-3 clientInfo contactNumber">
        ${client.contactnumber}
        </div>
        <div class="col-md-3 clientInfo edit" id="editButton">
        <a href="/amendClient.html?clientListID=${client.id}">
            <i class="fas fa-edit editButton"></i>
        </a>
    </div>
    </div>`

    }
}

//console.log("hihi");


window.onload = async (event) => {
    await loadClients()
}
