
const params = new URLSearchParams(window.location.search);
const clientListID = parseInt(params.get('clientListID'));

main();


async function main() {
    loadClientProfile(clientListID);

    async function loadClientProfile(clientListID) {
        const res = await fetch("/amendClient" + location.search);
        const result = (await res.json()).clientPageDetails;
        console.log(location.search)
        console.log(result)
        await displayClient(result)
    }

}

function displayClient(results) {
    document.querySelector('#clientNameInput').value = results.clientname;
    document.querySelector('#clientAddressInput').value = results.clientaddress;
    document.querySelector('#contactNumberInput').value = results.contactnumber;
    document.querySelector('#contactPersonInput').value = results.contactperson;
}



const editProfileForm = document.querySelector('#clientName')
editProfileForm.addEventListener("submit", async function (event) {
    event.preventDefault();

    const form = event.target;
    const formObj = {};

    formObj['clientName'] = form.clientName.value;
    formObj['clientAddress'] = form.clientAddress.value;
    formObj['contactNumber'] = form.contactNumber.value;
    formObj['contactPerson'] = form.contactPerson.value;


    console.log(formObj)

    const res = await fetch(`/profile/${clientListID}`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8"
        },
        body: JSON.stringify(formObj)
    })
    const result = await res.json();
    console.log(result)
    window.alert("Successfully update profile!")
})


