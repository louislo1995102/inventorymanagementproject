import express, { Request, Response} from 'express';
import {client} from './app'

export const clientRoutes = express.Router();

clientRoutes.get('/addClient',async function(req:Request,res:Response){
    const q = req.query.q;

    const result = await client.query('SELECT * FROM clientList');
    let clients = result.rows;
    
    console.log(clients);

    res.json(clients);
    
    // if(q){
    //     const result = await client.query('SELECT * FROM clientList WHERE content like $1',[`%${q}%`]);
    //     let clients= result.rows;
    //     res.json(clients);
    // }else{
    //     const result = await client.query('SELECT * FROM clientList');
    //     let clients = result.rows;
    //     res.json(clients);
    // }

    
});





clientRoutes.post('/addClient', async function(req:Request,res:Response){
    const clientName = req.body.clientName;
    const clientAddress = req.body.clientAddress;
    const contactPerson = req.body.contactPerson;
    const contactNumber = req.body.contactNumber;

    console.log(req.body)

    // await client.query('insert into clientList (clientName, clientAddress, contactPerson, contactNumber) values ($1,$2,$3,$4)',
    //         [clientName,clientAddress,contactPerson,contactNumber]);

    let a = await client.query('insert into clientList(clientName, clientAddress, contactPerson, contactNumber) values ($1, $2, $3 ,$4)' , [clientName,clientAddress,contactPerson,contactNumber]);

    console.log(a)
    res.status(200).json({success:true});
});
