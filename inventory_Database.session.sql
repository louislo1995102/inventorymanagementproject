
DROP TABLE items;


CREATE TABLE items (
id SERIAL PRIMARY KEY,
name VARCHAR(255) not null,
quantity INTEGER not null,
unit_price integer not null,
expired_date date,
image text,
status text,
entry_date date);


-- INSERT INTO items (name, quantity, price, expired_date, image)
--             VALUES('mooncake', 200, 10, '2022-04-18', 'imagefolder');

SELECT * FROM items;


-- CREATE TABLE stocks (
-- id SERIAL PRIMARY KEY,
-- item_id INTEGER,
-- status TEXT,
-- );


TRUNCATE TABLE items RESTART IDENTITY;

ALTER TABLE items ADD entry_date date;

select name, json_agg(unit_price) unit_price , json_agg(status) status
from items
group by 1
order by 1;




SELECT * FROM items WHERE expired_date < NOW();

SELECT * FROM items WHERE expired_date > NOW();

SELECT * FROM items WHERE status like '%Delivery%';