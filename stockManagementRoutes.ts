import express, { Request, Response } from 'express';
import { client } from './app';

export const stockManagementRoutes = express.Router();

stockManagementRoutes.get("/:productID", getProductDetail)

async function getProductDetail(req: Request, res: Response) {
    try {
        const productID = req.params.productID;
        const productDetail = (await client.query(`select name, price, image, status from product where productID.id = $1;`[productID])).rows;


    } catch (err) {
        console.error(err.message);
        res.status(400).json({ message: "Bad Request" })
    }
}

