

window.onload = () => {
    statusPage();
}


async function statusPage() {
    const res = await fetch('/groupData');
    const addproduct = await res.json();
    console.log(addproduct)
    const productStatus = document.querySelector('#statusPage');



    productStatus.innerHTML = '';
    for (let product of addproduct) {
        productStatus.innerHTML += `
        <div class="row my-container" style="margin-top: 5px; align-items: center;">${product.name}</div>

        <div class="row my-container" style="margin-top: 5px; align-items: center;">
            <div class="col-md-2">No.</div>
            <div class="col-md-2">Entry Date</div>
            <div class="col-md-2">Quantity</div>
            <div class="col-md-2">Expiry Date</div>
            <div class="col-md-2">Status</div>
            <div class="col-md-2">Action</div>
        </div>
        `
        for (let i = 0; i < product.unit_price.length; i++) {

            if (product.status[i] == "Delivery") {
                productStatus.innerHTML += `
        <div class="row my-container" style="margin-top: 5px; align-items: center;">
            <div class="col-md-2">${i + 1}</div>
            <div class="col-md-2">${moment(product.entry_date[i]).format("YYYY-MM-DD")}</div>
            <div class="col-md-2">${product.quantity[i]}</div>
            <div class="col-md-2">${moment(product.expired_date[i]).format("YYYY-MM-DD")}</div>
            <div class="col-md-2" class="productStatus"><font color="orange">${product.status[i]}</font></div>
            <div class="col-md-2"><a href="/deliveryList.html"><img src="/image/ProductStatus/dolly-solid.svg" style="width: 40%; box-shadow: 10px 10px 8px #888888;" alter="delivery-icon" class="img-fluid" /></a></div>
        </div>
        `
            } else if (product.status[i] == "Expired") {
                productStatus.innerHTML += `
            <div class="row my-container" style="margin-top: 5px; align-items: center;">
            <div class="col-md-2">${i + 1}</div>
            <div class="col-md-2">${moment(product.entry_date[i]).format("YYYY-MM-DD")}</div>
            <div class="col-md-2">${product.quantity[i]}</div>
            <div class="col-md-2">${moment(product.expired_date[i]).format("YYYY-MM-DD")}</div>
            <div class="col-md-2" class="productStatus"><font color="red">${product.status[i]}</font></div>
            <div class="col-md-2"><a href="/expiredItem.html"><img src="/image/ProductStatus/tasks-solid.svg" style="width: 40%; box-shadow: 10px 10px 8px #888888;" alter="delivery-icon" class="img-fluid" /></a></div>
        </div>
        `
            } else {
                productStatus.innerHTML += `
            <div class="row my-container" style="margin-top: 5px; align-items: center;">
            <div class="col-md-2">${i + 1}</div>
            <div class="col-md-2">${moment(product.entry_date[i]).format("YYYY-MM-DD")}</div>
            <div class="col-md-2">${product.quantity[i]}</div>
            <div class="col-md-2">${moment(product.expired_date[i]).format("YYYY-MM-DD")}</div>
            <div class="col-md-2" class="productStatus"><font color="black">${product.status[i]}</font></div>
            <div class="col-md-2"><a href="/invoiceList.html"><img src="/image/ProductStatus/edit-solid.svg" style="width: 40%; box-shadow: 10px 10px 8px #888888;" alter="delivery-icon" class="img-fluid" /></a></div>
        </div>
        `
            }

        }



        // const res = await fetch('/addproduct', {
        //     method: "POST",
        //     body: formData
        // });
        // const result = await res.json();
        // if (res.status == 200){
        //     console.log(result);
        // }
        //statusPage();
    }
}
