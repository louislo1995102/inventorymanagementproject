
window.onload = async () => {
    loadCurrentUser()
    await loadCalendar()
}


document.addEventListener('DOMContentLoaded', function () {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
        initialView: 'dayGridMonth',
        // initialDate: '2021-07-07',
        height: 600,
        headerToolbar: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay'
        }
    });
    calendar.render();
});





async function loadCalendar() {
    const res = await fetch('/invoiceDetails')

    const result = await res.json();
    console.log(result)

    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
        initialView: 'dayGridMonth',
        height: 600,
        headerToolbar: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay'
        },
        events: result.map(invoiceDetail => ({
            title: invoiceDetail.invoiceid,
            start: invoiceDetail.deliverydate,
            end: invoiceDetail.deliverydate,
        })),

    });
    calendar.render();
}

