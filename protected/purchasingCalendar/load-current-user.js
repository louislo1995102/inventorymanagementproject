

async function loadCurrentUser() {
    const res = await fetch('/current-user');


    const result = await res.json();
    console.log(result)
    if (result.username && document.querySelector('.login-pane')) {
        document.querySelector('.login-pane').innerHTML = `
        <div class="row mt-5 d-flex justify-content-center">
        <button type="submit" class="btn btn-secondary" style="width: 40%; height: 10%;">User ${result.username} has logged in</button>
        </div>
        <div class="row mt-5 d-flex justify-content-center">
        <button type="submit" class="btn btn-info" style="width: 40%; height: 10%;"  onclick="window.history.go(+1); return false;">Back to previous page</button>
        </div>`
    }
}