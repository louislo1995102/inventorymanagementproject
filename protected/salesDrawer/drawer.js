let drawer_open = false;

let menuPress = document.querySelectorAll('#menuButton');
menuPress[0].addEventListener("click", function () {
    console.log('test')
    cta_button_hide.play();
})

let backPress = document.querySelectorAll("#menuButton");
backPress[0].addEventListener("click", function () {
    if (drawer_open) {
        slidedown.play();
        cta_button_show.play();
    }
})

let cta_button_show = anime({
    targets: ['#menuButton', '#searchBar', '#dateTitle', '#clientNameTitle', '#applyFilter'],
    translateY: ['-15', '0'],
    opacity: ['0', '1'],
    easing: 'easeInOutSine',
    delay: anime.stagger(200),
    autoplay: false,
    // loop: false
    duration: 500,
    complete: function () {
    }
});


let cta_button_hide = anime({
    targets: ['#menuButton', '#searchBar'],
    translateY: ['0', '-15'],
    opacity: ['1', '0'],
    easing: 'easeInOutSine',
    delay: anime.stagger(200),
    autoplay: false,
    // loop: false
    duration: 500,
    complete: function () {
        slideup.play();
        drawer_open = true;
    }
});


let slidedown = anime({
    targets: '.container',
    translateY: ['-560', '-40px'],
    duartion: 1000,
    autoplay: false,
    begin: function () {
        show_hideCTA("block");
        drawer_open = false;
    }
})

let slideup = anime({
    targets: '.container',
    translateY: ['-40px', '-560px'],
    autoplay: false,
    begin: function () {
        show_hideCTA("none");
    }
})



function show_hideCTA(param) {
    document.querySelector("#menuButton").style.display = param;
    document.querySelector("#searchBar").style.display = param;
}