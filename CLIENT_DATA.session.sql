CREATE TABLE clientList(
    id SERIAL PRIMARY KEY,
    clientName varchar(255) not null,
    clientAddress varchar(255) not null,
    contactPerson varchar(255) not null,
    contactNumber INTEGER
)

insert into clientList(clientName, clientAddress,contactPerson,contactNumber) values
('ABC Restaurant','One Mid Town 27/F','Dr AWW', 68977700)

select * from clientList

TRUNCATE TABLE clientlist

DROP TABLE clientlist;


CREATE TABLE invoiceList(
    id SERIAL PRIMARY KEY,
    invoiceID INTEGER,
    clientName varchar(255) not null,
    clientAddress varchar(255) not null,
    product varchar(255) not null,
    productUnitAmount INTEGER,
    productUnitPrices INTEGER,
    deliveryDate DATE,
    TotalAmount INTEGER,
    Status TEXT
)

insert into invoiceList(invoiceID, clientName, clientAddress,product,productUnitAmount, productUnitPrices,deliveryDate,TotalAmount, Status) values
(210098,'ABC Restaurant','One Mid Town 27/F','mooncake',8,80, '2021-08-20',700,'delivery')

insert into invoiceList(invoiceID, clientName, clientAddress,product,productUnitAmount, productUnitPrices,deliveryDate,TotalAmount, Status) values
(210002,'DEG Restaurant','One Mid Town 27/F','mooncake',8,80, '2021-08-20',700,'delivery')

select * from invoiceList

TRUNCATE TABLE invoiceList
DROP TABLE invoiceList;


select sum(productUnitAmount) from invoiceList where product = 'cokacola';
